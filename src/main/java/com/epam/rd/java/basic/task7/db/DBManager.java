package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

    private static DBManager instance;
    //final String DATABASE_URL = "jdbc:postgresql://localhost:5432/test2db?user=postgres&password=NIKITA";
    Connection connection;

    public static synchronized DBManager getInstance() {
        try {
            if (instance == null) {
                instance = new DBManager();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return instance;
    }

    public DBManager() throws SQLException {
        connection = DriverManager.getConnection(getConnection());
    }

    public String getConnection() {
        Properties properties = new Properties();
        try(InputStream stream = Files.newInputStream(Paths.get("app.properties"))){
            properties.load(stream);
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return properties.getProperty("connection.url");
    }

    public List<User> findAllUsers() throws DBException {

        List<User> userList = new ArrayList<>();

        String sql = "SELECT* FROM users";

        try (PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet rs = statement.executeQuery()) {
//            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                userList.add(user);
            }
            return userList;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: findAllUsers()", e);
        }
    }

    public boolean insertUser(User user) throws DBException {

        //String sql = "INSERT INTO users(login) VALUES ('"+user.getLogin()+"')";
        String sql = "INSERT INTO users(login) VALUES (?)";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: insertUser()", e);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException{
        String sql = "DELETE FROM users WHERE login =?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (User u : users) {
                statement.setString(1, u.getLogin());
                statement.executeUpdate();
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: deleteUsers()", e);
        }

    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teamList = new ArrayList<>();

        String sql = "SELECT * FROM teams";

        try (PreparedStatement statement = connection.prepareStatement(sql);
         ResultSet resultSet = statement.executeQuery()) {


            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                teamList.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: findAllTeams()", e);
        }

        return teamList;
    }

    public boolean insertTeam(Team team) throws DBException {

        String sql = "INSERT INTO teams(name) VALUES (?)";
        //String sql = "INSERT INTO teams VALUES (?)";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, team.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: insertTeam()", e);
        }
        return true;
    }

    public User getUser(String login) throws DBException{
        String sql = "SELECT * FROM users WHERE users.login = ?";
        User user = null;

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            user = new User();
            user.setId(resultSet.getInt("id"));
            user.setLogin(resultSet.getString("login"));

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: getUser()", e);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        String sql = "SELECT * FROM teams WHERE teams.name = ?";
        Team team = null;

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            team = new Team();
            team.setId(resultSet.getInt("id"));
            team.setName(resultSet.getString("name"));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: getTeam()", e);
        }

        return team;
    }


    public boolean setTeamsForUser(User user, Team... teams) throws DBException {

        String sql = "INSERT INTO users_teams VALUES (?,?)";

        user.setId(getUser(user.getLogin()).getId());

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            connection.setAutoCommit(false);

            for (Team team : teams) {
                team.setId(getTeam(team.getName()).getId());
                statement.setInt(1,getUser(user.getLogin()).getId());
                statement.setInt(2,getTeam(team.getName()).getId());
                //statement.setInt(1, user.getId());
                //statement.setInt(2, team.getId());
                statement.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
            return true;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException roll) {
                roll.printStackTrace();
            }
            throw new DBException("failed", e);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teamList = new ArrayList<>();

        String sql = "SELECT * FROM teams LEFT JOIN users_teams ut on teams.id = ut.team_id where user_id = ?";

        //String sql = "SELECT * FROM users_teams WHERE user_id = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            //statement.setInt(1, user.getId());
            statement.setInt(1, getUser(user.getLogin()).getId());

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                teamList.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: getUserTeams()", e);
        }

        return teamList;
    }

    public boolean deleteTeam(Team team) throws DBException {

        String sql = "DELETE FROM teams WHERE teams.name = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            //statement.setString(1,getTeam(team.getName()).getName());
            statement.setString(1, team.getName());
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: deleteTeam()", e);
        }

    }

    public boolean updateTeam(Team team) throws DBException {

        //String sql = "UPDATE teams SET name = '" + team.getName() + "' WHERE teams.id = ?";
        String sql = "UPDATE teams SET name=? WHERE id=?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            statement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: updateTeam()", e);
        }
    }

}
